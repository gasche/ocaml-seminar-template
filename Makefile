.PHONY: all
all:
	dune build @pages

.PHONY: clean
clean:
	dune clean

.PHONY: install
# install simply copies the website content in a public/ subdirectory
install: all
	rm -fR public
	mkdir public
	cp *.html *.ics public/
	cp -r assets public/
	cp -r data public/
