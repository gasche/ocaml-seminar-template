This project is a template to create an OCaml-powered webpage for
a seminar.

# Workflow

You clone the template codebase, and then edit
[seminar.ml](seminar.ml) to add your "seminar data" (list of talks,
past and upcoming). Running the program then generates a HTML webpage
and a seminar.ics calendar file suitable for inclusion in calendar
clients.

Optionally, support is provided for automated deployment using Gitlab
Pages. This makes it easy to deploy the generated website (see below).


### Gitlab Pages workflow to deploy a seminar website

1. Clone the project as a new Gitlab repository, enable Gitlab CI and Gitlab Pages;
   the seminar website will be available as the corresponding Pages website.

   For example the template generates the demo website [Seminar Template](...).

2. Edit the seminar.ml file that contains the information about past
   and upcoming talks at your seminar.

   (A first time at the beginning, and then you edit it for new talks,
   schedule changes, to add links to seminar, etc.)

3. Whenever you push to your cloned repository, the website is
   regenerated with a webpage for the seminars, and a calendar.ics
   file suitable for inclusion in calendars.



# What is supported

- For each talk: title, speaker, date+duration (with reliable timezone support),
  location, (optional) abstract, optional slides/notes or additional links.

- As output formats: a simple HTML webpage, and a .ics calendar file.

- Customizable HTML output using the Mustache template language.



# What is not supported

- It's easy to tweak the generate the `index.html` HTML code for the
  website (just edit the [index.mustache](./index.mustache) template),
  but there is no easy support for including other files in the
  website, such as fancier CSS files, illustrations or whatever.

  (Should be rather easy to add.)

- Links to lecture slides/notes can be provided, but there is no
  specific support for hosting them directly in the seminar
  repository.

  (Should be rather easy to add.)

- Adding extra metadata on the talks requires modifying the OCaml datatypes.

  (Should be rather easy to do.)

  Feel free to open an issue here if you think your
  refinement/extension would be of interest to other users.

- No syndication feed (RSS/Atom)

  (Shoule be rather easy to do.)

- No automated generation of announce emails or mailing-list support.

  (Not sure whether that would really help. The current approach is
   for users to create a mailing-list for each seminar, and send
   announces manually.)


# Files

- [seminar.ml](./seminar.ml): the seminar datatype definition, and the seminar data
  (you need to edit this file to add new talks, etc.)
- [index.mustache](./index.mustache): Mustache template for the seminar webpage
  (you can edit this file to change the HTML output)
- [index.ml](./index.ml): seminar webpage generator
- [calendar.ml](./calendar.ml): seminar calendar generator
- [.gitlab-ci.yml](./.gitlab-ci.yml): the Gitlab CI configuration


# Building on

The iCalendar support is provided by the
[iCalendar](https://github.com/roburio/icalendar) library.

Dates are tricky; they are handled by the
[Timedesc](https://github.com/daypack-dev/timere) library.

The HTML output can be tweaked using the Mustache template language,
powered by the [ocaml-mustache](https://github.com/rgrinberg/ocaml-mustache/) library.
