open Seminar

let ptime_of_timedesc (v : Timedesc.t) : Ptime.t =
  let open Timedesc in
  (* Note: .to_timestamp_single will fail if given an ambiguous
     datatime that falls during a DST change period. *)
  Timedesc.to_timestamp_single v
  |> Utils.ptime_of_timestamp 
  |> Option.get

let ptime_span_of_span (v : Timedesc.Span.t) : Ptime.Span.t =
  Timedesc.Utils.ptime_span_of_span v
  |> Option.get

let global_organizer = uri "MAILTO:gabriel.scherer@inria.fr"
let global_tstamp = ptime_of_timedesc (Timedesc.now ())

let summary talk =
  Printf.sprintf "%s: %s"
    talk.speaker talk.title

let description talk =
  match talk.abstract with
  | None -> summary talk
  | Some abstract ->
    Printf.sprintf "%s\n\n%s"
      (summary talk) abstract

let noparams = Icalendar.Params.empty
let np x = (noparams, x)

let event talk : Icalendar.event = {
  dtstamp = np global_tstamp;
  uid = np talk.uid;
  dtstart = np (`Datetime (`Utc (ptime_of_timedesc talk.datetime)));
  dtend_or_duration  = Some (`Duration (np (ptime_span_of_span talk.duration)));
  rrule = None;
  props = [
    `Organizer (np global_organizer);
    `Location (np talk.location);
    `Summary (np (summary talk));
    `Description (np (description talk));
    `Class (np `Public);
  ] @ List.map (fun uri -> `Attach (np (`Uri uri))) talk.links;
  alarms = [];
}

let calendar (talks : talk list) : Icalendar.calendar =
  let cal_props = [
    `Prodid (np "-//Gabriel Scherer//Chameau sur le plateau//EN");
    `Version (np "2.0");
  ] in
  (cal_props, List.map (fun talk -> `Event (event talk)) talks)

let ics = Icalendar.to_ics (calendar (upcoming_talks @ previous_talks))

let () =
  print_endline ics
