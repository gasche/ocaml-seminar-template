type talk = {
  uid: string;
  (* This uid (Unique Identifier) should be unique among all talks in your seminar.
     Various data format (iCalendar, Atom, etc.) require unique identifiers for items,
     so keeping one in your data is good hygiene, even if it's slightly painful to come up
     with them when you enter the data manually.
     (There is a check in this file that the UIDs are indeed unique,
     so if you mistakenly introduce a conflict you will detect it at site-build time.)
  *)

  title: string;
  speaker: string;

  datetime: Timedesc.t;
  duration: Timedesc.Span.t;

  location: string;

  abstract: string option;

  slides: Uri.t option;
  notes: Uri.t option;
  links: Uri.t list;
}

let uri = Uri.of_string

let seminar_timezone =
  Timedesc.Time_zone.make "Europe/Paris"
  |> Option.get

let datetime (year, month, day) (hour, minute) =
  let month = Timedesc.Utils.human_int_of_month month in
  Timedesc.make
    ~tz:seminar_timezone
    ~year ~month ~day
    ~hour ~minute ~second:0
    ()
  |> Result.get_ok

let default_duration =
  let minutes n = n in
  let talk_duration = minutes 50 in
  let question_duration = minutes 10 in
  let coffee_duration = minutes 30 in
  Timedesc.Span.For_human.make
    ~minutes:(
      talk_duration
      + question_duration
      + coffee_duration
    ) ()
  |> Result.get_ok

let online =
  let url = "https://bbb.example.com/ocaml-rocks-g2j-q9m" in
  Printf.sprintf {|Online: <a href="%s">%s</a>|}
    url url

let univ room =
  Printf.sprintf
    "Example university,\n\
     Alan Turing building,\n\
     room %s"
    room

let doc rel_url =
  uri ("doc/" ^ rel_url)

let upcoming_talks : talk list = [
  {
    title = "Totally awesome future talk";
    speaker = "Florian Angeletti";
    datetime = datetime (2021, `Jun, 10) (10, 00);
    duration = default_duration;
    uid = "florian-angeletti_awesome-talk";

    location = online;

    abstract = Some {|
Florian is going to give an awesome imaginary talk on *something*.
|};

    slides = None;
    notes = None;
    links = [];
  };

]

let previous_talks : talk list = [
  {
    title = "Some recent work";
    speaker = "Gabriel Scherer";
    datetime = datetime (2021, `Mar, 15) (10, 00);
    duration = default_duration;
    uid = "gabriel-scherer_recent-work";

    location = online;

    abstract = Some
{|
In this talk Gabriel will present recent work on Foo Bar.
No previous knowledge of Foo is expected, we will start with a gentle introduction.
|};

    slides = Some (doc "README.md");
    notes = Some (doc "README.md");
    links = [
      uri "https://hal.archives-ouvertes.fr/hal-03146495/";
    ];
  };

  {
    title = "The first talk of the example series";
    speaker = "Damien Doligez";
    datetime = datetime (2019, `Oct, 08) (10, 30);
    duration = default_duration;
    uid = "damien-doligez_first-talk";

    location = univ "Marcel-Paul Schützenberger (2024)";

    abstract = None;
    slides = None;
    notes = None;
    links = [];
  };
]

let () =
  let all_talks = upcoming_talks @ previous_talks in
  let sorted_uids =
    all_talks
    |> List.map (fun talk -> talk.uid)
    |> List.sort String.compare in
  let rec check = function
    | [] | [_] -> ()
    | u1 :: ((u2 :: _) as rest) ->
      if String.equal u1 u2 then
        Printf.ksprintf failwith "UID conflict on %S" u1;
      check rest
  in
  check sorted_uids
