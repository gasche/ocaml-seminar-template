open Seminar

let show_uri u = Uri.to_string u

let show_datetime (datetime : Timedesc.t) =
  Timedesc.to_string ~format:"{wday:Xx*}, {mon:Xx*} {day:X}, {year}" datetime
  |> Option.get

let timere_compare_datetime (d1 : Timedesc.t) d2 =
  let d1, d2 = Timedesc.(to_timestamp_float_s_single d1, to_timestamp_float_s_single d2) in
  Float.compare d1 d2

let json_of_talk ~is_next_talk talk =
  let str v = `String v in
  let opt to_str v = Option.fold (Option.map to_str v) ~none:`Null ~some:str in
  `O (
    [
      ("uid", str talk.uid);
      ("title", str talk.title);
      ("speaker", str talk.speaker);
      ("datetime", str (show_datetime talk.datetime));
      ("location", str talk.location);
      ("slides", opt show_uri talk.slides);
      ("notes", opt show_uri talk.notes);
      ("is-next-talk", opt Fun.id (if is_next_talk then Some "true" else None));
    ]
  )

let json_of_previous_talks =
  let previous_talks =
    previous_talks
    (* sort the talks, most recent first *)
    |> List.stable_sort (fun t1 t2 -> timere_compare_datetime t2.datetime t1.datetime)
  in
  `A (List.map (json_of_talk ~is_next_talk:false) previous_talks)

let json_of_upcoming_talks =
  let upcoming_talks =
    upcoming_talks
    (* sort the talks, closest in the future first *)
    |> List.stable_sort (fun t1 t2 -> timere_compare_datetime t1.datetime t2.datetime)
  in
  match upcoming_talks with
  | [] -> `A []
  | first :: others ->
    (* now we reverse to get the closest in the future last *)
    `A (List.rev_append
          (List.map (json_of_talk ~is_next_talk:false) others)
          [json_of_talk ~is_next_talk:true first])

let json_of_seminar =
  `O [
    "upcoming-talks", json_of_upcoming_talks;
    "previous-talks", json_of_previous_talks;
  ]

let get_template path =
  let chan = open_in path in
  Fun.protect ~finally:(fun () -> close_in chan) @@ fun () ->
  let lexbuf = Lexing.from_channel chan in
  Lexing.set_filename lexbuf path;
  Mustache.parse_lx lexbuf

let () =
  Mustache.render_fmt
    ~strict:true
    ~partials:(fun path ->
      Some (get_template (path ^ ".mustache"))
    )
    Format.std_formatter
    (get_template "index.mustache")
    json_of_seminar;
  Format.pp_print_flush Format.std_formatter ()
